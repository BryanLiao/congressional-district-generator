package org.bats.FairMander.web;

import org.bats.FairMander.structs.CongressionalDistrict;
import org.bats.FairMander.structs.Precinct;
import java.util.ArrayList;


public class ChangeLog {
   public ArrayList<ChangeEntry> changes = new ArrayList<ChangeEntry>();
   
   public void clear(){
       changes.clear();
   }

   public void addEntry(Precinct p, CongressionalDistrict cD1, CongressionalDistrict cD2, double d) {
       changes.add(new ChangeEntry(p,cD1,cD2,d));
   }
}