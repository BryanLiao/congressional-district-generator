package org.bats.FairMander.structs;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class State implements Serializable {

  @Id
  @Column(name = "statename", unique = true, nullable = false)
  String name;
  @Transient
  ArrayList<CongressionalDistrict> districts;
  @Transient
  Statistics stateStats;
  @Transient
  String geoJson;

  public State() {
    super();
  }

  public State(String name, ArrayList<CongressionalDistrict> districts, Statistics stateStats, String geoJson) {
    super();
    this.name = name;
    this.districts = districts;
    this.stateStats = stateStats;
    this.geoJson = geoJson;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArrayList<CongressionalDistrict> getDistricts() {
    return districts;
  }

  public void setDistricts(ArrayList<CongressionalDistrict> districts) {
    this.districts = districts;
  }

  public Statistics getStateStats() {
    return stateStats;
  }

  public void setStateStats(Statistics stateStats) {
    this.stateStats = stateStats;
  }

  public String getGeoJson() {
    return geoJson;
  }

  public void setGeoJson(String geoJson) {
    this.geoJson = geoJson;
  }

  public CongressionalDistrict getDistrictById(int id) {
    return districts.get(id-1);
  }
  public Precinct getPrecinctById(int id,int year){
      for(int i = 0; i < districts.size();i++){
          for(int j = 0;j < districts.get(i).getPrecinctsByYear(year).size();j++){
              if(districts.get(i).getPrecinctsByYear(year).get(j).getId()== id)
                  return districts.get(i).getPrecinctsByYear(year).get(j);
          }
      }
      return null;
  }
  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

}
