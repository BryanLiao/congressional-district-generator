package org.bats.FairMander.data;

import java.util.List;
import org.bats.FairMander.structs.ElectionResults;
import org.springframework.data.repository.CrudRepository;

public interface ElectionResultsRepository extends CrudRepository<ElectionResults,Integer> {
    List<ElectionResults> findByStatisticsId(int statisticsId);
}
