package org.bats.FairMander.structs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:generalproperties.properties")
public class GoodnessCalculator {

    @Autowired
    private ApplicationContext appContext;

    public enum measureStrings {
        PARTISANFAIRNESS,
        POLSBYPOPPERCOMPACTNESS,
        SOCIOECONOMIC,
        GEOGRAPHIC

    }
    @Value("${goodnessMeasures}")
    String measureName;
    Collection<Constraint> constraints = new ArrayList<Constraint>();
    ArrayList<Measure> measures = new ArrayList<Measure>();
    Statistics stateStats;
    Map<String, Integer> weights;
    private double goodness;
    HashMap<String, Class<?>> measureMap = new HashMap<String, Class<?>>();
    private int congressionalDistrictAmount;

    public double calculateGoodness(CongressionalDistrict cd, int year) {
        goodness = 0;
        ArrayList<Constraint> c = (ArrayList<Constraint>) constraints;
        for (int i = 0; i < c.size(); i++) {
            goodness += -10 * c.get(i).calculateConstraint(stateStats, cd, year, congressionalDistrictAmount);
        }
        for (int i = 0; i < measures.size(); i++) {
            Integer measureWeight = weights.get(measures.get(i).getName());
            if (measureWeight != null) {
                double goodnessCalculation = measures.get(i).calculate(stateStats, cd, year,congressionalDistrictAmount);
                goodness += measureWeight * goodnessCalculation;
            }
        }

        return goodness;
    }

    public void setWeights(Map<String, Integer> map) {
        weights = map;
    }

    public void setStatistics(Statistics s) {
        stateStats = s;
    }

    public void setMeasures(ArrayList<Measure> m) {
        measures = m;
    }

    public void setConstraints(Statistics statistics) {
        PopulationConstraint populationConstraint = appContext.getBean(PopulationConstraint.class);
        constraints.add(populationConstraint);
    }

    public int getCongressionalDistrictAmount() {
        return congressionalDistrictAmount;
    }

    public void setCongressionalDistrictAmount(int congressionalDistrictAmount) {
        this.congressionalDistrictAmount = congressionalDistrictAmount;
    }

    void setMeasuresWithParams(HashMap<String, Integer> params) {
        measureMap.put("Polsby Popper Compactness",PolsbyPopperCompactnessMeasure.class);
        
        String[] measureNames = measureName.split(",");
        for (String str : measureNames) {
            if (params.get(str) != null) {
                if (measureMap.get(str) != null) {
                    measures.add((Measure)appContext.getBean((Class) measureMap.get(str)));
                }
            }
        }
    }

}
