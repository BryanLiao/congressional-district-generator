package org.bats.FairMander.structs;

import com.vividsolutions.jts.geom.Geometry;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Statistics implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    @Column
    long population;
    @Column
    int precinctId;

    @Transient
    RacialDemographic racialDemographics;
    @Transient
    SocioEconomicDemographic socioEconomicDemographics;
    @Transient
    HashMap<String, Long> partisanAllegiances;
    @Transient
    ElectionResults electionResults;
    @Transient
    ArrayList<Geometry> geometry = new ArrayList<Geometry>();
    @Transient
    Geometry shape;

    public Statistics() {
        super();
    }

    public Statistics(long population, RacialDemographic racialDemographics,
            SocioEconomicDemographic socioEconomicDemographics, HashMap<String, Long> partisanAllegiances,
            ElectionResults electionResults, ArrayList<Geometry> geometry) {
        super();
        this.population = population;
        this.racialDemographics = racialDemographics;
        this.socioEconomicDemographics = socioEconomicDemographics;
        this.partisanAllegiances = partisanAllegiances;
        this.electionResults = electionResults;
        this.geometry = geometry;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public RacialDemographic getRacialDemographic() {
        return racialDemographics;
    }

    public void setRacialDemographic(RacialDemographic racialDemographics) {
        this.racialDemographics = racialDemographics;
    }

    public SocioEconomicDemographic getSocioEconomicDemographics() {
        return socioEconomicDemographics;
    }

    public void setSocioEconomicDemographics(SocioEconomicDemographic socioEconomicDemographics) {
        this.socioEconomicDemographics = socioEconomicDemographics;
    }

    public HashMap<String, Long> getPartisanAllegiances() {
        return partisanAllegiances;
    }

    public void setPartisanAllegiances(HashMap<String, Long> partisanAllegiances) {
        this.partisanAllegiances = partisanAllegiances;
    }

    public ElectionResults getElectionResults() {
        return electionResults;
    }

    public void setElectionResults(ElectionResults electionResults) {
        this.electionResults = electionResults;
    }

    public void appendStatistics(Statistics s) {
        population += s.getPopulation();
        this.getElectionResults().append(s.getElectionResults());
        this.getRacialDemographic().append(s.getRacialDemographic());
        if (this.shape != null) {
            for (int i = 0; i < s.getGeometry().size(); i++) {
                this.shape.union(s.getGeometry().get(i));
            }
        }
        this.geometry.addAll(s.getGeometry());
    }

    public void removeStatistics(Statistics s) {
        population -= s.getPopulation();
        this.getElectionResults().remove(s.getElectionResults());
        this.getRacialDemographic().remove(s.getRacialDemographic());
        this.getGeometry().removeAll(s.getGeometry());
    }

    public ArrayList<Geometry> getGeometry() {
        return geometry;
    }

    public void setGeometry(ArrayList<Geometry> geometry) {
        this.geometry = geometry;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Geometry getShape() {
        return shape;
    }

    public void setShape(Geometry shape) {
        this.shape = shape;
    }

    public int getPrecinctId() {
        return precinctId;
    }

    public void setPrecinctId(int precinctId) {
        this.precinctId = precinctId;
    }

}
