package org.bats.FairMander.structs;

import com.vividsolutions.jts.geom.Geometry;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class CongressionalDistrict implements Serializable {

  @Id
  @Column
  int id;
  @Column
  String name;
  @Column
  String statename;
  @Transient
  HashMap<Integer, ArrayList<Precinct>> precincts;
  @Transient
  HashMap<Integer, Statistics> stats;
  @Transient
  boolean locked;
  @Column
  double goodness;
  @Transient
  Geometry shape;

  public CongressionalDistrict() {
      HashMap<Integer, ArrayList<Precinct>> precincts = new HashMap<Integer,ArrayList<Precinct>>();
      stats = new HashMap<Integer,Statistics>();
  }

  public CongressionalDistrict(int id, String name, HashMap<Integer, ArrayList<Precinct>> precincts, boolean locked,
          double goodness) {
    super();
    this.id = id;
    this.name = name;
    this.precincts = precincts;
    this.locked = locked;
    this.goodness = goodness;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

    public String getStatename() {
        return statename;
    }

    public void setStatename(String statename) {
        this.statename = statename;
    }

  public HashMap<Integer, ArrayList<Precinct>> getPrecincts() {
    return precincts;
  }

  public void setPrecincts(HashMap<Integer, ArrayList<Precinct>> precincts) {
    this.precincts = precincts;
  }

  public void setPrecinctsByYear(int year, ArrayList<Precinct> precincts) {
    this.precincts.put(new Integer(year), precincts);
  }

  public boolean getLocked() {
    return locked;
  }

  public void setLocked(boolean locked) {
    this.locked = locked;
  }

  public double getGoodness() {
    return goodness;
  }

  public void setGoodness(double goodness) {
    this.goodness = goodness;
  }


  //Methods
  public boolean verify() {
    return false;
  }

  public void setStatisticsByYear(int year, Statistics stat) {
    this.stats.put(year,stat);
  }

  public Statistics getStatisticsByYear(int year) {
    return stats.get(year);
  }

  public ArrayList<Precinct> getOuterBorderPrecinctsByYear(int year) {
    ArrayList<Precinct> outerBorder = new ArrayList<Precinct>();
    for (Precinct precinct : precincts.get(year)) {
      if (precinct.isBorder()) {
        for (Precinct p : precinct.getAdjacentPrecincts()) {
          if (precinct.getCongressionalDistrictId() != p.getCongressionalDistrictId()) {
            if (!outerBorder.contains(p)) {
              outerBorder.add(p);
            }
          }
        }
      }
    }
    return outerBorder;
  }

 
  public void removePrecinct(Precinct precinct) {
    precincts.get(precinct.getYear()).remove(precinct);
    getStatisticsByYear(precinct.getYear()).removeStatistics(precinct.getStats());

    ArrayList<Precinct> precincts = precinct.getAdjacentPrecincts();
    for (int i = 0; i < precincts.size(); i++) {
      ArrayList<Precinct> adjacentPrecincts = precincts.get(i).getAdjacentPrecincts();
      boolean borderbool = false;
      for (int j = 0; j < adjacentPrecincts.size(); j++) {
        if (adjacentPrecincts.get(j).getCongressionalDistrictId() != id) {
          borderbool = true;
          adjacentPrecincts.get(j).setBorder(borderbool);
          break;
        } else {
          adjacentPrecincts.get(j).setBorder(borderbool);
        }
      }
    }
  }
  

  public void addPrecinct(Precinct precinct) {
    precincts.get(precinct.getYear()).add(precinct);
    
    //Contiguity handler(edge case 1)
    for(int i = 0; i< precinct.getAdjacentPrecincts().size();i++){
        if(precinct.getAdjacentPrecincts().get(i).getAdjacentPrecincts().size() == 1){
            if(precinct.getAdjacentPrecincts().get(i).getAdjacentPrecincts().get(0).equals(precinct)){
                precincts.get(precinct.getYear()).add(precinct.getAdjacentPrecincts().get(i));
                getStatisticsByYear(precinct.getYear()).appendStatistics(precinct.getAdjacentPrecincts().get(i).getStats());
                precinct.getAdjacentPrecincts().get(i).setCongressionalDistrictId(id);
            }
        }
    }
    
    getStatisticsByYear(precinct.getYear()).appendStatistics(precinct.getStats());
    precinct.setCongressionalDistrictId(id);
    ArrayList<Precinct> precincts = precinct.getAdjacentPrecincts();
    for (int i = 0; i < precincts.size(); i++) {
      ArrayList<Precinct> adjacentPrecincts = precincts.get(i).getAdjacentPrecincts();
      boolean borderbool = false;
      for (int j = 0; j < adjacentPrecincts.size(); j++) {
        if (adjacentPrecincts.get(j).getCongressionalDistrictId() != id) {
          borderbool = true;
          adjacentPrecincts.get(j).setBorder(borderbool);
          break;
        } else {
          adjacentPrecincts.get(j).setBorder(borderbool);
        }
      }
    }
  }

  public ArrayList<Precinct> getPrecinctsByYear(int year) {
    return precincts.get(year);
  }
public HashMap<Integer,String> getPrecinctGeoJSON(int year){
    HashMap<Integer,String> precinctsMap = new HashMap<Integer,String>();
    ArrayList<Precinct> prect = this.getPrecinctsByYear(year);
    for(int i = 0; i<prect.size();i++){
        precinctsMap.put(prect.get(i).getId(),prect.get(i).getGeoJson());
    }
    return precinctsMap;
}
}
