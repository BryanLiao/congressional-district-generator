package org.bats.FairMander.structs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:generalproperties.properties")
public class PopulationConstraint extends Constraint {

    @Value("${populationDeviation}")
    private final double deviation = .001;

    @Override
    public double calculateConstraint(Statistics s, CongressionalDistrict cd, int year, int cdAmount) {
        long averagePopulation = s.getPopulation() / cdAmount;
        long districtPopulation = cd.getStatisticsByYear(year).getPopulation();
        if ((districtPopulation > (long) (averagePopulation * (1.0 + deviation)))) {
            double j = Math.abs(districtPopulation - averagePopulation);
            return j;
        }

        if (districtPopulation < (long) (averagePopulation * (1.0 - deviation))) {
            double j = Math.abs(districtPopulation - averagePopulation);
            return j;
        }
        return 0;
    }
}
