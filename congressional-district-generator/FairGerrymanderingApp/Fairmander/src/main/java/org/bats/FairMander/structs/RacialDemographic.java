package org.bats.FairMander.structs;

import java.io.Serializable;
import javax.persistence.*;

@Entity
public class RacialDemographic implements Serializable {

    @Column
    @Id
    int id;
    @Column
    long americanIndian;
    @Column
    long asian;
    @Column
    long black;
    @Column
    long hawaiian;
    @Column
    long hispanic;
    @Column
    long other;
    @Column
    long white;
    @Column
    int statisticsId;

    public RacialDemographic() {
        super();
    }

    public RacialDemographic(int id, long americanIndian, long asian, long black, long hawaiian, long hispanic, long other, long white, int statisticsId) {
        this.id = id;
        this.americanIndian = americanIndian;
        this.asian = asian;
        this.black = black;
        this.hawaiian = hawaiian;
        this.hispanic = hispanic;
        this.other = other;
        this.white = white;
        this.statisticsId = statisticsId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getAmericanIndian() {
        return americanIndian;
    }

    public void setAmericanIndian(long americanIndian) {
        this.americanIndian = americanIndian;
    }

    public long getAsian() {
        return asian;
    }

    public void setAsian(long asian) {
        this.asian = asian;
    }

    public long getBlack() {
        return black;
    }

    public void setBlack(long black) {
        this.black = black;
    }

    public long getHawaiian() {
        return hawaiian;
    }

    public void setHawaiian(long hawaiian) {
        this.hawaiian = hawaiian;
    }

    public long getHispanic() {
        return hispanic;
    }

    public void setHispanic(long hispanic) {
        this.hispanic = hispanic;
    }

    public long getOther() {
        return other;
    }

    public void setOther(long other) {
        this.other = other;
    }

    public long getWhite() {
        return white;
    }

    public void setWhite(long white) {
        this.white = white;
    }

    public int getStatisticsId() {
        return statisticsId;
    }

    public void setStatisticsId(int statisticsId) {
        this.statisticsId = statisticsId;
    }

    public void append(RacialDemographic rd) {
        this.americanIndian += rd.getAmericanIndian();
        this.asian += rd.getAsian();
        this.black += rd.getBlack();
        this.hawaiian += rd.getHawaiian();
        this.hispanic += rd.getHispanic();
        this.white += rd.getWhite();
        this.other += rd.getOther();
    }

    public void remove(RacialDemographic rd) {
        this.americanIndian -= rd.getAmericanIndian();
        this.asian -= rd.getAsian();
        this.black -= rd.getBlack();
        this.hawaiian -= rd.getHawaiian();
        this.hispanic -= rd.getHispanic();
        this.white -= rd.getWhite();
        this.other -= rd.getOther();
    }

    public String getLabels() {
        return "['American-Indian', 'Asian', 'Black', 'Hawaiian', 'Hispanic', 'White', 'Other']";
    }

    public String getPercentages() {
        return "[" + this.americanIndian + "," + this.asian + "," + this.black + "," + this.hawaiian + "," + this.hispanic + "," + this.white + "," + this.other + "]";
    }

}
