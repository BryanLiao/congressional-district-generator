package org.bats.FairMander.UserElements;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Controller
public class UserController {

  @Autowired
  UserRepository userRepository;

  //ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
  //HttpSession session = attr.getRequest().getSession(true);
  User usr;

  @RequestMapping(value = "/login", method = RequestMethod.POST)
  @ResponseBody
  public String logIn(@RequestBody Object params) {
    HashMap<String, String> credentials = (HashMap<String, String>) params;
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    HttpSession session = request.getSession(true);
    if (!userRepository.findById(credentials.get("userName")).isPresent()) {
      String response;
      response = "{\"status\":\"Unauthorized\"}";
      return response;
    } else {
      String response;
      User user = userRepository.findById(credentials.get("userName")).get();
      if (user.getPassword1().equals(credentials.get("password"))) {
        session.setAttribute("user", user);
        user.setLoggedIn(true);
        if (user.getAdmin() == 1) {
          response = "{\"status\":\"Admin\",\"username\" : \""+user.getUsername()+"\",\"name\" : \"" + user.getName() + "\"}";
        } else {
          response = "{\"status\":\"User\",\"username\" : \""+user.getUsername()+"\",\"name\" : \"" + user.getName() + "\"}";
        }
        return response;
      } else {
        response = "{\"status\":\"Unauthorized\"}";
        return response;
      }
    }
  }

  @RequestMapping(value = "/logout", method = RequestMethod.POST)
  @ResponseBody
  public String logout() {
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    HttpSession session = request.getSession(true);
    User sessionUser = (User) session.getAttribute("user");
    if (sessionUser == null) {
      return "{\"status\":\"fail\"}";
    } else {
      User user = userRepository.findById(sessionUser.getUsername()).get();
      user.setLoggedIn(false);
      sessionUser = null;
      session.setAttribute("user", null);
      return "{\"status\":\"success\"}";
    }
  }

  @RequestMapping(value = "/register", method = RequestMethod.POST)
  @ResponseBody
  public String register(@RequestBody Object usr) {
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    HttpSession session = request.getSession(true);
    HashMap<String, String> mappedUser = (HashMap<String, String>) usr;
    String uname = mappedUser.get("userName");
    String pword = mappedUser.get("password");
    String name = mappedUser.get("name");
    String email = mappedUser.get("email");
    if ((userRepository.findById(uname).isPresent())) {
      return "{\"status\":\"Username_Taken!\"}";
    } else {
      User registeringUser = new User();
      registeringUser.setUsername(uname);
      registeringUser.setPassword(pword);
      registeringUser.setName(name);
      registeringUser.setEmail(email);
      registeringUser.setLoggedIn(true);
      registeringUser.setAdmin(0);
      userRepository.save(registeringUser);
      session.setAttribute("user", registeringUser);
      return "{\"status\":\"success\",\"name\" : \"" + name + "\"}";
    }
  }
  
  @RequestMapping(value = "/user", method = RequestMethod.POST)
  @ResponseBody
  public String getUser(){
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    HttpSession session = request.getSession(true);
    User user;
    if(session.getAttribute("user") != null){
      user=(User)session.getAttribute("user");
      if(user.getAdmin()==1)
      return "{\"status\":\"Admin\",\"name\" : \""+user.getName()+"\",\"username\" : \""+user.getUsername()+"\"}";
      else if(user.isLoggedIn()==true){
      return "{\"status\":\"User\",\"name\" : \""+user.getName()+"\",\"username\" : \""+user.getUsername()+"\"}";
      }
    }
    return "{\"status\":\"anon\"}";
  }
  
  @RequestMapping(value = "/account", method = RequestMethod.POST)
  @ResponseBody
  public String changeAccount(@RequestBody Object acctInfo){
    HashMap<String, String> acctMap = (HashMap<String, String>) acctInfo;
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    HttpSession session = request.getSession(true);
    User user;
      user=(User)session.getAttribute("user");
      String uname=acctMap.get("username");
      String name=acctMap.get("name");
      String pass=acctMap.get("oldPassword");
      if(userRepository.findById(uname).isPresent())
      {
      User userJpa = userRepository.findById(user.getUsername()).get();
      if(pass.equals(userJpa.getPassword1())){
        user.setName(acctMap.get("name"));
        userJpa.setName(acctMap.get("name"));
        user.setPassword1(acctMap.get("newPassword"));
        userJpa.setPassword1(acctMap.get("newPassword"));
        userRepository.save(userJpa);
        return "{\"password\":\"success\"}";
      }
      return "{\"password\":\"failed\"}";
    }
    return "{\"password\":\"failed\"}";
  }

    @RequestMapping(value = "/editUser", method = RequestMethod.POST)
  @ResponseBody
   public String editUser(@RequestBody Object param) {
       HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
       HttpSession session = request.getSession(true);
       HashMap<String, String> user = (HashMap<String, String>) param;
       if (userRepository.findById(user.get("userName")).isPresent()) {
                    User usr = userRepository.findById(user.get("userName")).get();
           if(!(user.get("email").equals(new String("")))){
               usr.setEmail(user.get("email"));
           }
           else if(!(user.get("password").equals(new String("")))){
               usr.setPassword(user.get("password"));
           }
           else if(!(user.get("name").equals(new String("")))){
               usr.setName(user.get("name"));
           }
           userRepository.save(usr);
           return "{\"status\":\"edited\",\"username\":\""+user.get("userName")+"\"}";
       } else {
           String response;
           response = "{\"status\":\"No such User\"}";
           return response;
       }
   }
       @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
       @ResponseBody
   public String deleteUser(@RequestBody String param) {
       HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
       HttpSession session = request.getSession(true);
       String username = param;
       if (!userRepository.findById(username).isPresent()) {
           String response;
           response = "{\"status\":\"No\"}";
           return response;
       } else {
           User usr = userRepository.findById(username).get();
           userRepository.delete(usr);
           return "{\"status\":\"deleted\",\"username\":\""+username+"\"}";
       }
   }
   
  @RequestMapping(value = "/addUser", method = RequestMethod.POST)
  @ResponseBody
  public String addUser(@RequestBody Object usr) {
    HashMap<String, String> mappedUser = (HashMap<String, String>) usr;
    String uname = mappedUser.get("userName");
    String pword = mappedUser.get("password");
    String name = mappedUser.get("name");
    String email = mappedUser.get("email");
    if ((userRepository.findById(uname).isPresent())) {
      return "{\"status\":\"Username_Taken!\"}";
    } else {
      User registeringUser = new User();
      registeringUser.setUsername(uname);
      registeringUser.setPassword(pword);
      registeringUser.setName(name);
      registeringUser.setEmail(email);
      registeringUser.setLoggedIn(true);
      registeringUser.setAdmin(0);
      userRepository.save(registeringUser);
      return "{\"status\":\"success\",\"name\" : \"" + name + "\",\"username\" : \"" + uname + "\"}";
    }
  }

}

