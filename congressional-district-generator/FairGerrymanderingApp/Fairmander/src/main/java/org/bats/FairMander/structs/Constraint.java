package org.bats.FairMander.structs;

public abstract class Constraint {
    String name;
    
    public abstract double calculateConstraint(Statistics s, CongressionalDistrict cd, int year, int cdAmount);
}
