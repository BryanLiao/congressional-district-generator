package org.bats.FairMander.structs;

import java.io.Serializable;
import java.util.HashMap;
import javax.persistence.*;

@Entity
public class ElectionResults implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  int id;
  @Column
  int statisticsId;
  @Column
  float republican;
  @Column
  float democratic;
  @Column
  float green;
  @Column
  float libertarian;
  @Column
  float other;
 public ElectionResults(){
     super();
 }
    public ElectionResults(int id, int statisticsId, float republican, float democratic, float green, float libertarian, float other) {
        this.id = id;
        this.statisticsId = statisticsId;
        this.republican = republican;
        this.democratic = democratic;
        this.green = green;
        this.libertarian = libertarian;
        this.other = other;
    }
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getStatisticsId() {
    return statisticsId;
  }

  public void setStatisticsId(int statisticsId) {
    this.statisticsId = statisticsId;
  }

  public float getGreen() {
    return green;
  }

  public void setGreen(float green) {
    this.green = green;
  }

  public float getLibertarian() {
    return libertarian;
  }

  public void setLibertarian(float libertarian) {
    this.libertarian = libertarian;
  }

  public float getOther() {
    return other;
  }

  public void setOther(float other) {
    this.other = other;
  }

  public float getRepublican() {
    return republican;
  }

  public void setRepublican(float republican) {
    this.republican = republican;
  }

  public float getDemocratic() {
    return democratic;
  }

  public void setDemocratic(float democratic) {
    this.democratic = democratic;
  }
  
  public HashMap<String,Float> getResultsAsHashMap(){
    HashMap<String,Float> results = new HashMap<String,Float>();
    
    results.put("republican", republican);
    results.put("democratic", democratic);
    results.put("green", green);
    results.put("libertarian", libertarian);
    
    return results;
  }
  public void append(ElectionResults er){
      this.republican += er.getRepublican();
      this.democratic += er.getDemocratic();
      this.green += er.getGreen();
      this.libertarian += er.getLibertarian();
      this.other += er.getOther();
  }
    public void remove(ElectionResults er){
      this.republican -= er.getRepublican();
      this.democratic -= er.getDemocratic();
      this.green -= er.getGreen();
      this.libertarian -= er.getLibertarian();
      this.other -= er.getOther();
  }
}
