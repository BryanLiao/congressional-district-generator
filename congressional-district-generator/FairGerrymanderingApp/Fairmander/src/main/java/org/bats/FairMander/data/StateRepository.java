package org.bats.FairMander.data;

import org.bats.FairMander.structs.State;
import org.springframework.data.repository.CrudRepository;

public interface StateRepository extends CrudRepository<State,String>{
    
}
