package org.bats.FairMander.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.geom.Geometry;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.bats.FairMander.structs.CongressionalDistrict;
import org.bats.FairMander.structs.ElectionResults;
import org.bats.FairMander.structs.Precinct;
import org.bats.FairMander.structs.RacialDemographic;
import org.bats.FairMander.structs.State;
import org.bats.FairMander.structs.Statistics;
import org.geotools.geojson.feature.FeatureJSON;
import org.json.simple.JSONObject;
import org.opengis.feature.simple.SimpleFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Controller
@PropertySource("classpath:generalproperties.properties")
public class DataController {

    @Autowired
    private StateRepository stateRepository;
    @Autowired
    private CongressionalDistrictRepository congressionalDistrictRepository;
    @Autowired
    private PrecinctRepository precinctRepository;
    @Autowired
    private ElectionResultsRepository electionResultsRepository;
    @Autowired
    private StatisticRepository statisticRepository;
    @Autowired
    private RacialDemographicRepository racialDemographicRepository;

    //ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
    //HttpSession session = attr.getRequest().getSession(true);
    State currentState;
    private final int MAX_MEASURES = 4;
    @Value("${title}")
    private final String title = "CSE308 - TEAM BATS";
    @Value("${clientname}")
    private final String clientname = "";
    @Value("${password}")
    private final String password = "";
    @Value("${login}")
    private final String login = "Login";
    @Value("${register}")
    private final String register = "Register";
    @Value("${statenames}")
    private final String statenames = "Massachusetts,Arkansas,Connecticut";
    @Value("${goodness}")
    private final String goodness = "";
    @Value("${goodnessMeasures}")
    private final String goodnessMeasures = "";
    @Value("${apply}")
    private final String apply = "Apply";
    @Value("${defaultYear}")
    private final int defaultYear = 2016;

    ObjectMapper om = new ObjectMapper();
    @RequestMapping("/state")
    public String getState(@RequestParam(value = "statename", required = true) String statename, Map<String, Object> model) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        session.setAttribute("Year", this.defaultYear);
        currentState = stateRepository.findById(statename).get();
        if (currentState == null) {
            return "{\"name\":\"InvalidState\"}";
        }

        if (!isStateNameValid(statename, model)) {
            return "{\"name\":\"InvalidState\"}";
        }

//        model.put("title", this.title);
//        model.put("clientname", this.clientname);
//        model.put("password", this.password);
//        model.put("login", this.login);
//        model.put("register", this.register);
//        model.put("goodness", this.goodness);
//        model.put("title", this.title);
//        model.put("apply", this.apply);

    model.put("title", "tittiles");
    model.put("clientname", "tittiles");
    model.put("password", "tittiles");
    model.put("login", "tittiles");
    model.put("register", "tittiles");
    model.put("goodness", "tittiles");
    model.put("title", "tittiles");
    model.put("apply", "tittiles");
        setupModelMeasures(this.goodnessMeasures, model);

        setUpState(statename);

        return "state";
    }

   @RequestMapping(value = "/cdData", method = RequestMethod.POST)
   @ResponseBody
   public String getCDData(@RequestBody int cdid) {
       HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
       HttpSession session = request.getSession(true);
       State currState = (State) session.getAttribute("modifiedState");
       Statistics stats = currState.getDistrictById(cdid).getStatisticsByYear((int) session.getAttribute("Year"));
       String racialPercentage = stats.getRacialDemographic().getPercentages();
       String racialLabel = stats.getRacialDemographic().getLabels();
       return "{\"racialPercentage\":\""+racialPercentage+"\",\"racialComp\" : \""+racialLabel+"\",\"population\" : \"" + stats.getPopulation() + "\""
               + ",\"democratic\" : \"" + stats.getElectionResults().getDemocratic() + "\",\"republican\" : \"" + stats.getElectionResults().getRepublican() + "\",\"libertarian\" : \"" + stats.getElectionResults().getLibertarian() + "\""
               + ",\"green\" : \"" + stats.getElectionResults().getGreen() + "\",\"other\" : \"" + stats.getElectionResults().getOther() + "\"}";

   }
   
    @RequestMapping("/stateData")
    public String getStateData(Map<String, Object> model) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        State currState = (State) session.getAttribute("modifiedState");
        Statistics stats = currState.getStateStats();
        String racialPercentage = stats.getRacialDemographic().getPercentages();
        String racialLabel = stats.getRacialDemographic().getLabels();
        //Object o =  
        //String racial = "[39, 22, 19, 12, 10]" ; 
        //String raciallabel = "[\"1-20000\", \"20000-40000\", \"40000-60000\", \"60000-80000\", \"80000-100000+\"]"; 
        model.put("racialPercentage", racialPercentage);
        model.put("racialComp", racialLabel);
        model.put("statename", currState.getName());
        model.put("population", stats.getPopulation());
        model.put("cdnum", "Number Of Congressional Districts: " + currState.getDistricts().size());
        model.put("democratic", stats.getElectionResults().getDemocratic());
        model.put("republican", stats.getElectionResults().getRepublican());
        model.put("libertarian", stats.getElectionResults().getLibertarian());
        model.put("green", stats.getElectionResults().getGreen());
        model.put("other", stats.getElectionResults().getOther());
        return "stateData";
    }

    @RequestMapping("/stateDataCompare")
    public String getStateDataCompare() {
        return "stateDataCompare";
    }

    @RequestMapping("/congressionalDistrict")
    @ResponseBody
    public String getCongressionalDistrict() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        State currState = (State) session.getAttribute("modifiedState");
        int year = (int) session.getAttribute("Year");
        //Precinct p = currState.getPrecinctById(pnum, year);
       // String resp = "{\"cdid\": \""+p.getCongressionalDistrictId()+"\", \"geojson\" : \""+p.getGeoJson()+"\" }";
            HashMap<Integer, int[]> response = new HashMap<Integer,int[]>(); //HashMap<Integer, String>>();
        for (CongressionalDistrict cd : currState.getDistricts()) {
            int[] array  = new int[cd.getPrecinctsByYear(year).size()];
            for(int i = 0; i <cd.getPrecinctsByYear(year).size();i++){
                array[i] = cd.getPrecinctsByYear(year).get(i).getId();
            }
            response.put(cd.getId(),array);
        }
        String json;
        try {
            json = om.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            json = e.getMessage();
        }
        //json.replaceAll("\"","\\\"");
            return json;
    }

    private void setUpState(String statename) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        int year = (int) session.getAttribute("Year");
        
        List<CongressionalDistrict> congressionalDistricts = congressionalDistrictRepository.findByStatename(statename);
        currentState.setDistricts((ArrayList<CongressionalDistrict>) congressionalDistricts);
        Statistics statestats = new Statistics(0, new RacialDemographic(0, 0, 0, 0, 0, 0, 0, 0, 0), null, null, new ElectionResults(0, 0, 0, 0, 0, 0, 0), new ArrayList<Geometry>());
        for (CongressionalDistrict congressionalDistrict : congressionalDistricts) {
            congressionalDistrict.setPrecincts(new HashMap<Integer, ArrayList<Precinct>>());
            List<Precinct> precincts = precinctRepository.findByCongressionalDistrictIdAndYear(congressionalDistrict.getId(), year);
            congressionalDistrict.setPrecinctsByYear(year, (ArrayList<Precinct>) precincts);
            Statistics cdstats = new Statistics(0, new RacialDemographic(0, 0, 0, 0, 0, 0, 0, 0, 0), null, null, new ElectionResults(0, 0, 0, 0, 0, 0, 0), new ArrayList<Geometry>());
            for (Precinct precinct : precincts) {
                Statistics statistics = statisticRepository.findByPrecinctId(precinct.getId() / 10000).get(0);
                precinct.setStatistics(statistics);
                precinct.setGeoJson(precinct.getGeoJson().replace('\'', '\"'));
                Reader reader;
                try {
                    reader = new StringReader(precinct.getGeoJson());
                    FeatureJSON fjson = new FeatureJSON();
                    SimpleFeature feat = fjson.readFeature(reader);
                    Geometry geo = (Geometry) feat.getDefaultGeometry();
                    statistics.getGeometry().add(geo);
                    if (cdstats.getShape() == null) {
                        cdstats.setShape(geo);
                    } else {
                        cdstats.setShape(cdstats.getShape().union(geo));
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                statistics.setElectionResults(electionResultsRepository.findByStatisticsId(statistics.getId()).get(0));
                statistics.setRacialDemographic(racialDemographicRepository.findByStatisticsId(statistics.getId()).get(0));
                cdstats.appendStatistics(precinct.getStats());
            }
            congressionalDistrict.setStatisticsByYear(year, cdstats);
            statestats.appendStatistics(cdstats);
        }
        currentState.setStateStats(statestats);
        setupAdjacency(currentState, year);
        session.setAttribute("defaultState", currentState);
        State modifiedState = new State();
        try {
            modifiedState = (State) currentState.clone();
            session.setAttribute("modifiedState", modifiedState);
        } catch (CloneNotSupportedException ex) {
            Statistics statestat = new Statistics(0, new RacialDemographic(0, 0, 0, 0, 0, 0, 0, 0, 0), null, null, new ElectionResults(0, 0, 0, 0, 0, 0, 0), new ArrayList<Geometry>());
            ArrayList<CongressionalDistrict> cdlist = new ArrayList<CongressionalDistrict>();
            for (CongressionalDistrict cd : currentState.getDistricts()) {
                CongressionalDistrict mCD = new CongressionalDistrict();
                HashMap<Integer, ArrayList<Precinct>> plist = new HashMap<Integer, ArrayList<Precinct>>();
                mCD.setPrecincts(plist);
                Statistics cdstat = new Statistics(0, new RacialDemographic(0, 0, 0, 0, 0, 0, 0, 0, 0), null, null, new ElectionResults(0, 0, 0, 0, 0, 0, 0), new ArrayList<Geometry>());
                ArrayList<Precinct> precincts = new ArrayList<Precinct>();
                for (Precinct p : cd.getPrecinctsByYear(year)) {
                    ElectionResults er = new ElectionResults(p.getStats().getElectionResults().getId(), p.getStats().getElectionResults().getStatisticsId(), p.getStats().getElectionResults().getRepublican(), p.getStats().getElectionResults().getDemocratic(), p.getStats().getElectionResults().getGreen(), p.getStats().getElectionResults().getLibertarian(), p.getStats().getElectionResults().getOther());
                    RacialDemographic rd = new RacialDemographic(p.getStats().getRacialDemographic().getId(), p.getStats().getRacialDemographic().getAmericanIndian(), p.getStats().getRacialDemographic().getAsian(), p.getStats().getRacialDemographic().getBlack(), p.getStats().getRacialDemographic().getHawaiian(), p.getStats().getRacialDemographic().getHispanic(), p.getStats().getRacialDemographic().getOther(), p.getStats().getRacialDemographic().getWhite(), p.getStats().getRacialDemographic().getStatisticsId());
                    Statistics stats = new Statistics();
                    stats.setElectionResults(er);
                    stats.setPopulation(p.getStats().getPopulation());
                    stats.setRacialDemographic(rd);
                    Precinct pct = new Precinct();
                    pct.setStatistics(stats);
                    pct.setName(p.getName());
                    //pct.setAdjacentPrecincts((ArrayList<Precinct>) p.getAdjacentPrecincts().clone());
                    pct.setId(p.getId());
                    pct.setIncumbent(p.isIncumbent());
                    pct.setCongressionalDistrictId(cd.getId());
                    pct.setYear(year);
                    pct.setBorder(p.isBorder());
                    pct.setGeoJson(p.getGeoJson());
                    Reader reader = new StringReader(p.getGeoJson());
                    try {
                        FeatureJSON fjson = new FeatureJSON();
                        Geometry geo = (Geometry) fjson.readFeature(reader).getDefaultGeometry();
                        stats.getGeometry().add(geo);
                        if (cdstat.getShape() == null) {
                            cdstat.setShape(geo);
                        } else {
                            cdstat.setShape(cdstat.getShape().union(geo));
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    pct.setAdjacentPrecinctIDs(p.getAdjacentPrecinctIDs());
                    precincts.add(pct);
                    cdstat.appendStatistics(pct.getStats());
                }
                mCD.setStatisticsByYear(year, cdstat);
                statestat.appendStatistics(cdstat);
                mCD.setGoodness(cd.getGoodness());
                mCD.setId(cd.getId());
                mCD.setName(cd.getName());
                mCD.setStatename(currentState.getName());
                mCD.setLocked(cd.getLocked());
                mCD.setPrecinctsByYear(year, precincts);
                cdlist.add(mCD);
            }
            modifiedState.setStateStats(statestat);
            modifiedState.setName(currentState.getName());
            modifiedState.setDistricts(cdlist);
            setupAdjacency(modifiedState, year);
            session.setAttribute("modifiedState", modifiedState);
        }
    }

    private boolean isStateNameValid(String statename, Map<String, Object> model) {
        String[] tokens = this.statenames.split(",");

        for (String token : tokens) {
            if (token.equals(statename)) {
                model.put("statename", statename);
                return true;
            }
        }
        return false;
    }

    private void setupAdjacency(State s, int year) {
        ArrayList<Precinct> masterlist = new ArrayList<Precinct>();
        for (CongressionalDistrict cd : s.getDistricts()) {
            masterlist.addAll(cd.getPrecinctsByYear(year));
        }
        for (int i = 0; i < masterlist.size(); i++) {
            if (masterlist.get(i).getAdjacentPrecinctIDs() == null) {
                continue;
            }
            String[] adjacentIds = masterlist.get(i).getAdjacentPrecinctIDs().split(",");
            ArrayList<Precinct> adjacentPrecincts = new ArrayList<Precinct>();
            for (int j = 0; j < adjacentIds.length; j++) {
                if (adjacentIds[j].equals("")) {
                    continue;
                }
                int identif = Integer.parseInt(adjacentIds[j]);
                for (int k = 0; k < masterlist.size(); k++) {
                    if (masterlist.get(k).getId() / 10000 == identif) {
                        if (masterlist.get(i).getCongressionalDistrictId() != masterlist.get(k).getCongressionalDistrictId()) {
                            masterlist.get(i).setBorder(true);
                        }
                        adjacentPrecincts.add(masterlist.get(k));
                        break;
                    }
                }
            }
            masterlist.get(i).setAdjacentPrecincts(adjacentPrecincts);
        }
    }

    private void setupModelMeasures(String goodnessMeasures, Map<String, Object> model) {
        String[] goodnesstokens = goodnessMeasures.split(",");
        for (int i = 0; i < goodnesstokens.length; i++) {
            if (i > MAX_MEASURES) {
                break;
            }
            String temp = "gm" + (i);
            model.put(temp, goodnesstokens[i]);
        }
    }
}
