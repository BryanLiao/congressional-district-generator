package org.bats.FairMander.structs;

import com.vividsolutions.jts.geom.Geometry;
import java.util.ArrayList;


public class SchwartzbergCompactnessMeasure extends Measure {
    String name = "Schwartzberg Compactness";
    
    public double calculate(Statistics s, CongressionalDistrict cd, int year,int districtNumber){
        
        Geometry merged = cd.getStatisticsByYear(year).getShape();
        double r = Math.sqrt(merged.getArea()/Math.PI);
        double equalAreaPerimeter = 2 * Math.PI * r;
        double score = 1 / (merged.getLength()/equalAreaPerimeter);
        return score;
    }
    public String getName(){
        return name;
    }
}
