package org.bats.FairMander.structs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.vividsolutions.jts.geom.Geometry;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.bats.FairMander.web.ChangeLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import static org.springframework.http.RequestEntity.method;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Controller
public class AlgorithmController {

    @Autowired
    private ApplicationContext appContext;

    //ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
    //HttpSession session = attr.getRequest().getSession(true);
    //State modifiedState = new State();//(State) session.getAttribute("modifiedState");
    GoodnessCalculator gc;
    ChangeLog changes = new ChangeLog();
    ObjectMapper om = new ObjectMapper();

    @RequestMapping("/testAlgorithm")
    @ResponseBody
    public String testAlgorithm(){
        HashMap<String,Integer> params = new HashMap<String,Integer>();
        params.put("Polsby Popper Compactness",1);
        //params.put("Population", 1);
        startAlgo(params);
        String changelog = new String("");
        for(int i=1;i<=9;i++){
            changelog += "\nRun "+ i + ": "+ runAlgorithm(i);
        }
        return changelog;
    }

    @RequestMapping(value="/startAlgo", method = RequestMethod.POST)
    @ResponseBody
    public String startAlgo(@RequestBody Object params) {
      HashMap<String, Integer> credentials = (HashMap<String, Integer>) params;
      HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
      HttpSession session = request.getSession(true);
      return "{\"ready\":\"" + startAlgorithm(credentials, session) + "\"}";
    }
    public boolean startAlgorithm(HashMap<String, Integer> params, HttpSession session) {
        State modifiedState =(State) session.getAttribute("modifiedState");
        GoodnessCalculator goodnessCalculator = appContext.getBean(GoodnessCalculator.class);
        goodnessCalculator.setCongressionalDistrictAmount(modifiedState.getDistricts().size());
        goodnessCalculator.setWeights(params);
        goodnessCalculator.setStatistics(modifiedState.getStateStats());
        goodnessCalculator.setMeasuresWithParams(params);
        goodnessCalculator.setConstraints(modifiedState.getStateStats());

        session.setAttribute("goodnessCalculator", goodnessCalculator);
        gc = goodnessCalculator;
        changes.clear();

        return true;
    }

    @RequestMapping(value = "/runAlgo", method = RequestMethod.POST)
    @ResponseBody
    public String runAlgorithm(@RequestBody int cdid) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        State modifiedState =(State) session.getAttribute("modifiedState");
        int year =(int) session.getAttribute("Year");
        GoodnessCalculator goodnessCalculator = gc;//(GoodnessCalculator) session.getAttribute("goodnessCalculator");
        CongressionalDistrict cD1 = modifiedState.getDistrictById(cdid);
        ArrayList<Precinct> precincts = cD1.getOuterBorderPrecinctsByYear(year);
        for (Precinct precinct : precincts) {
            CongressionalDistrict cD2 = modifiedState.getDistrictById(precinct.getCongressionalDistrictId());

            if (!(cD2.getLocked() || cD1.getLocked())) {
                if ((cD1.getGoodness() == 0) || (cD2.getGoodness() == 0)) {
                    cD1.setGoodness(goodnessCalculator.calculateGoodness(cD1, year));
                    cD2.setGoodness(goodnessCalculator.calculateGoodness(cD2, year));
                }

                double before = cD1.getGoodness() + cD2.getGoodness();
                Geometry cD1Shape = cD1.getStatisticsByYear(year).getShape();
                Geometry cD2Shape = cD2.getStatisticsByYear(year).getShape();
                swapDistricts(precinct, cD1, cD2);

                double after1 = goodnessCalculator.calculateGoodness(cD1, year);
                double after2 = goodnessCalculator.calculateGoodness(cD2, year);
                if (before < after1 + after2) {
                    cD1.setGoodness(after1);
                    cD2.setGoodness(after2);
                    //Compactness optimization
                    changes.addEntry(precinct, cD1, cD2, (after1 + after2) - before);
                } else {
                    swapDistricts(precinct, cD2, cD1);
                    cD1.getStatisticsByYear(year).setShape(cD1Shape);
                    cD2.getStatisticsByYear(year).setShape(cD2Shape);
                }
            }
        }
        String json;
        try {
            json = om.writeValueAsString(changes);
        } catch (JsonProcessingException e) {
            json = e.getMessage();
        }
        changes.clear();
        return json;

    }
    @RequestMapping("/manualMove")
    public String manualMove(@RequestParam(value = "precinctId") int precinctId, @RequestParam(value = "districtId") int districtId)
    {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);
        State modifiedState =(State) session.getAttribute("modifiedState");
        int year =(int) session.getAttribute("Year");
        Precinct p = modifiedState.getPrecinctById(precinctId, year);
        swapDistricts(p,modifiedState.getDistrictById(districtId),modifiedState.getDistrictById(p.getCongressionalDistrictId()));
        return "";
    }
    private void swapDistricts(Precinct p, CongressionalDistrict cD1, CongressionalDistrict cD2) {
        cD1.addPrecinct(p);
        cD2.removePrecinct(p);
    }

}
