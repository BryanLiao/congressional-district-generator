package org.bats.FairMander.data;

import java.util.List;
import org.bats.FairMander.structs.Precinct;
import org.springframework.data.repository.CrudRepository;

public interface PrecinctRepository extends CrudRepository<Precinct,Integer> {
    List<Precinct> findByCongressionalDistrictIdAndYear(int congressionalDistrictId,int year);
}
