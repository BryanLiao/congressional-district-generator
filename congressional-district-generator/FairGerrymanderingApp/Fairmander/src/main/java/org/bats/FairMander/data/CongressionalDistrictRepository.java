/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bats.FairMander.data;

import java.util.List;
import org.bats.FairMander.structs.CongressionalDistrict;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Thomas
 */
public interface CongressionalDistrictRepository extends CrudRepository<CongressionalDistrict,Integer> {
    List<CongressionalDistrict> findByStatename(String statename);
}
