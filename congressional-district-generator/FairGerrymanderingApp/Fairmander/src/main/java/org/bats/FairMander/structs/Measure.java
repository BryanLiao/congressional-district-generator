package org.bats.FairMander.structs;

public abstract class Measure {
    
    String name;
    
    public abstract double calculate(Statistics s, CongressionalDistrict cd, int year,int districtNumber);
    public abstract String getName();
}
