package org.bats.FairMander.web;

import org.bats.FairMander.structs.CongressionalDistrict;
import org.bats.FairMander.structs.Precinct;


class ChangeEntry {
   //Provides Only Names and Ids to be sent to front
   public String precinct;
   public int taker;
   public int giver;
   public double goodness;
   ChangeEntry(Precinct p, CongressionalDistrict cD1, CongressionalDistrict cD2, double g) {
       precinct = p.getName();
       taker = cD1.getId()+1;
       giver = cD2.getId()+1;
       goodness = g;
   }
   
}