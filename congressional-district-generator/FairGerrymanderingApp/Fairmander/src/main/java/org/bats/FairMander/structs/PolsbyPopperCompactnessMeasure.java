package org.bats.FairMander.structs;

import com.vividsolutions.jts.geom.Geometry;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
@Component
@PropertySource("classpath:generalproperties.properties")
public class PolsbyPopperCompactnessMeasure extends Measure {
    
    String name = "Polsby Popper Compactness";
    public double calculate(Statistics s, CongressionalDistrict cd, int year, int districtNumber){
        Geometry merged = cd.getStatisticsByYear(year).getShape();
        double polsbyPopperScore = (4 * Math.PI * merged.getArea()) / Math.pow(merged.getLength(), 2);
        return polsbyPopperScore;
    }
    public String getName(){
        return name;
    }
}
