package org.bats.FairMander.structs;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.*;

@Entity
public class Precinct implements Serializable {

  @Id
  int id;
  @Column
  String name;
  @Column
  int congressionalDistrictId;
  @Column
  String adjacentPrecinctIDs;
  @Transient
  Statistics stats;
  @Transient
  ArrayList<Precinct> adjacentPrecincts;
  @Column
  boolean incumbent;
  @Column
  boolean isBorder;
  @Transient
  boolean locked;
  @Column
  String geoJson;
  @Column
  int year;

  public Precinct() {
    super();
  }

  public Precinct(int id, String name, Statistics stats, String adjacentPrecincts, boolean incumbent,
          boolean locked, String geoJson) {
    super();
    this.id = id;
    this.name = name;
    this.stats = stats;
    this.adjacentPrecinctIDs = adjacentPrecincts;
    this.incumbent = incumbent;
    this.locked = locked;
    this.geoJson = geoJson;
  }

    public String getAdjacentPrecinctIDs() {
        return adjacentPrecinctIDs;
    }

    public void setAdjacentPrecinctIDs(String adjacentPrecinctIDs) {
        this.adjacentPrecinctIDs = adjacentPrecinctIDs;
    }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCongressionalDistrictId() {
    return congressionalDistrictId;
  }

  public void setCongressionalDistrictId(int congressionalDistrictId) {
    this.congressionalDistrictId = congressionalDistrictId;
  }

  public Statistics getStats() {
    return stats;
  }

  public void setStatistics(Statistics stats) {
    this.stats = stats;
  }

  public ArrayList<Precinct> getAdjacentPrecincts() {
    return adjacentPrecincts;
  }

  public void setAdjacentPrecincts(ArrayList<Precinct> adjacentPrecincts) {
    this.adjacentPrecincts = adjacentPrecincts;
  }

  public boolean isIncumbent() {
    return incumbent;
  }

  public void setIncumbent(boolean incumbent) {
    this.incumbent = incumbent;
  }

  public boolean isLocked() {
    return locked;
  }

  public void setLocked(boolean locked) {
    this.locked = locked;
  }

  public String getGeoJson() {
    return geoJson;
  }

  public void setGeoJson(String geoJson) {
    this.geoJson = geoJson;
  }

  public boolean isBorder() {
    return isBorder;
  }

  public void setBorder(boolean isBorder) {
    this.isBorder = isBorder;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

}
