package org.bats.FairMander.data;

import java.util.List;
import org.bats.FairMander.structs.RacialDemographic;
import org.springframework.data.repository.CrudRepository;

public interface RacialDemographicRepository extends CrudRepository<RacialDemographic,Integer> {
    List<RacialDemographic> findByStatisticsId(int statisticsId);
}
