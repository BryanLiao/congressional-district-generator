/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bats.FairMander;

import com.vividsolutions.jts.geom.Geometry;
import java.util.ArrayList;

/**
 *
 * @author Thomas
 */
public class PrecinctGeometry {
    String id;
    Geometry geom;
    ArrayList<PrecinctGeometry> adjacents = new ArrayList<PrecinctGeometry>();
    public PrecinctGeometry(String identif,Geometry g){
        id = identif;
        geom = g;
    }
    public ArrayList<PrecinctGeometry> getAdjacents(){
        return adjacents;
    }
    public void addToAdjacents(PrecinctGeometry g){
        if(adjacents.contains(g)){
            return;
        }
        else if(this.equals(g)){
            return;
        }
        else{
            adjacents.add(g);
            g.addToAdjacents(this);
        }
    }
    public Geometry getGeometry(){
        return geom;
    }
}
