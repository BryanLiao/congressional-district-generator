package org.bats.FairMander;

import com.vividsolutions.jts.geom.Geometry;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.opengis.feature.simple.SimpleFeature;

public class AdjacencyGenerator {

    public SimpleFeature feature;
    public static ArrayList<PrecinctGeometry> geolist = new ArrayList<PrecinctGeometry>();

    public static void convertGeoJSON(String filepath) {
        FeatureJSON fjson = new FeatureJSON();
        try {
            Reader reader;
            List<String> lines = Files.readAllLines(Paths.get(filepath));
            for (String line : lines) {
                reader = new StringReader(line.substring(0, line.length() - 1));
                SimpleFeature g = fjson.readFeature(reader);
                Geometry geo = (Geometry) g.getDefaultGeometry();
                geolist.add(new PrecinctGeometry(g.getAttribute("OBJECTID").toString(),geo));
            }
        } catch (Exception e) {
            String go = e.getMessage();
        }
    }
    public static void calculateAdjacency(){
        for(int i = 0; i < geolist.size();i++){
            for(int j = 0; j < geolist.size();j++)
            {
                if(geolist.get(i).getGeometry().intersects(geolist.get(j).getGeometry()))
                    geolist.get(i).addToAdjacents(geolist.get(j));
            }
        }
    }
    public static String getAdjacencyTable(){
        String result = "";
        for(int i = 0; i< geolist.size();i++){
            result+=geolist.get(i).id;
            result+="\t\t\t{";
            for(int j = 0;j < geolist.get(i).getAdjacents().size();j++){
                result+=geolist.get(i).getAdjacents().get(j).id;
                if(j!=geolist.get(i).getAdjacents().size()-1)
                    result+=",";
            }
            result+="}\n";
        }
        return result;
    }

}
