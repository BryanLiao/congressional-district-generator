package org.bats.FairMander.UserElements;

import java.io.Serializable;
import java.util.HashMap;
import javax.persistence.*;

import org.bats.FairMander.structs.State;

@Entity
public class User implements Serializable {

  @Id
  @Column(unique = true)
  String username;
  @Transient
  Byte[] password;
  @Column
  String password1;
  @Column
  String name;
  @Column
  String email;
  @Column
  int admin;
  @Transient
  HashMap<String, State> savedStates;
  @Column
  boolean loggedIn;

  public User() {
    super();
  }

  public User(String username, Byte[] password, int admin,
          HashMap<String, State> savedStates, boolean loggedIn) {
    super();
    this.username = username;
    this.password = password;
    this.admin = admin;
    this.savedStates = savedStates;
    this.loggedIn = loggedIn;
  }
  
  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Byte[] getPassword() {
    return password;
  }

  public void setPassword(Byte[] password) {
    this.password = password;
  }

  public void setPassword(String password) {
    this.password1 = password;
  }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

 
  public int getAdmin() {
    return admin;
  }

  public void setAdmin(int admin) {
    this.admin = admin;
  }

  public HashMap<String, State> getSavedStates() {
    return savedStates;
  }

  public void setSavedStates(HashMap<String, State> savedStates) {
    this.savedStates = savedStates;
  }

  public boolean isLoggedIn() {
    return loggedIn;
  }

  public void setLoggedIn(boolean loggedIn) {
    this.loggedIn = loggedIn;
  }

}
