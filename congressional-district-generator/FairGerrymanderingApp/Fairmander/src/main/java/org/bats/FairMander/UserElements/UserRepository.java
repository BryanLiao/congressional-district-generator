package org.bats.FairMander.UserElements;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,String>{
    
}
