package org.bats.FairMander.structs;

import java.util.HashMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@Component
@PropertySource("classpath:generalproperties.properties")
public class PartisanMeasure extends Measure{
    
    @Value("${partisanFairnessMultiplier}")
    private double multiplier;
    public String name = "Political Fairness";
    @Override
    public double calculate(Statistics s, CongressionalDistrict cd, int year,int districtNumber) {
        float wastedVotes = getWastedVotes(cd.getStatisticsByYear(year));
        return wastedVotes*multiplier;
    }
    
    private float getWastedVotes(Statistics s) {
        HashMap<String,Float> electionResults = s.getElectionResults().getResultsAsHashMap();
        
        float maxVotes=electionResults.get("republican");
        String winner = "republican";
        float sum = electionResults.get("republican");
        for (String key : electionResults.keySet()){
          if(electionResults.get(key)>maxVotes){
            maxVotes = electionResults.get(key);
            winner = key;
          }
          sum = sum + electionResults.get(key);
        }
        
        return (float)((maxVotes/sum-.5)*sum + sum-maxVotes);
        
        
    }
    
    public String getName(){
        return name;
    }
}
