package org.bats.FairMander.data;

import java.util.List;
import org.bats.FairMander.structs.Statistics;
import org.springframework.data.repository.CrudRepository;

public interface StatisticRepository extends CrudRepository<Statistics,Integer> {
    List<Statistics> findByPrecinctId(int precinctId);
}
