
package org.bats.FairMander.structs;

public class RacialFairnessMeasure extends Measure{
    String name = "Racial Fairness";
    private double deviation = .05;
    public String getName(){
        return name;
    }
    @Override
    public double calculate(Statistics s, CongressionalDistrict cd, int year,int districtNumber) {
        RacialDemographic rd = s.getRacialDemographic();
        RacialDemographic cdrd = cd.getStatisticsByYear(year).getRacialDemographic();
        double goodness = 1000;
        if(cdrd.getAmericanIndian()< ((rd.getAmericanIndian()/districtNumber)*(1-deviation))){
            goodness = goodness *(cdrd.getAmericanIndian()/((rd.getAmericanIndian()/districtNumber)*(1-deviation)));
        }
        else if(cdrd.getAmericanIndian()> ((rd.getAmericanIndian()/districtNumber)*(1+deviation))){
            goodness = goodness *(((rd.getAmericanIndian()/districtNumber)*(1+deviation))/cdrd.getAmericanIndian());
        }
        if(cdrd.getAsian()< ((rd.getAsian()/districtNumber)*(1-deviation))){
            goodness = goodness *(cdrd.getAsian()/((rd.getAsian()/districtNumber)*(1-deviation)));
        }
        else if(cdrd.getAsian()> ((rd.getAsian()/districtNumber)*(1+deviation))){
            goodness = goodness *(((rd.getAsian()/districtNumber)*(1+deviation))/cdrd.getAsian());
        }
        if(cdrd.getBlack()< ((rd.getBlack()/districtNumber)*(1-deviation))){
            goodness = goodness *(cdrd.getBlack()/((rd.getBlack()/districtNumber)*(1-deviation)));
        }
        else if(cdrd.getBlack()> ((rd.getBlack()/districtNumber)*(1+deviation))){
            goodness = goodness *(((rd.getBlack()/districtNumber)*(1+deviation))/cdrd.getBlack());
        }
        if(cdrd.getHawaiian()< ((rd.getHawaiian()/districtNumber)*(1-deviation))){
            goodness = goodness *(cdrd.getHawaiian()/((rd.getHawaiian()/districtNumber)*(1-deviation)));
        }
        else if(cdrd.getHawaiian()> ((rd.getHawaiian()/districtNumber)*(1+deviation))){
            goodness = goodness *(((rd.getHawaiian()/districtNumber)*(1+deviation))/cdrd.getHawaiian());
        }
        if(cdrd.getHispanic()< ((rd.getHispanic()/districtNumber)*(1-deviation))){
            goodness = goodness *(cdrd.getHispanic()/((rd.getHispanic()/districtNumber)*(1-deviation)));
        }
        else if(cdrd.getHispanic()> ((rd.getHispanic()/districtNumber)*(1+deviation))){
            goodness = goodness *(((rd.getHispanic()/districtNumber)*(1+deviation))/cdrd.getHispanic());
        }
        if(cdrd.getOther()< ((rd.getOther()/districtNumber)*(1-deviation))){
            goodness = goodness *(cdrd.getOther()/((rd.getOther()/districtNumber)*(1-deviation)));
        }
        else if(cdrd.getOther()> ((rd.getOther()/districtNumber)*(1+deviation))){
            goodness = goodness *(((rd.getOther()/districtNumber)*(1+deviation))/cdrd.getOther());
        }
        if(cdrd.getWhite()< ((rd.getWhite()/districtNumber)*(1-deviation))){
            goodness = goodness *(cdrd.getWhite()/((rd.getWhite()/districtNumber)*(1-deviation)));
        }
        else if(cdrd.getWhite()> ((rd.getWhite()/districtNumber)*(1+deviation))){
            goodness = goodness *(((rd.getWhite()/districtNumber)*(1+deviation))/cdrd.getWhite());
        }
        return goodness;
    }
}
