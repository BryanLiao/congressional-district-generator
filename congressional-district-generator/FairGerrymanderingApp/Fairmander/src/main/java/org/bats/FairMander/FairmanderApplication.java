package org.bats.FairMander;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FairmanderApplication {

	public static void main(String[] args) {
//            AdjacencyGenerator.convertGeoJSON("F:\\Users\\PropOfThomas\\Desktop\\CSE 308-1\\BATS\\FairGerrymanderingApp\\Fairmander\\src\\main\\resources\\static\\revised2.geojson");
//            AdjacencyGenerator.calculateAdjacency();
//            System.out.println(AdjacencyGenerator.getAdjacencyTable());
            SpringApplication.run(FairmanderApplication.class, args);
	}
}
