/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
app.config(['$routeProvider', function (rp) {
    rp
            .when('/', {templateUrl: 'stateData'})
            .when('/compare', {templateUrl: 'stateDataCompare'})
            .when('/congDistrict', {templateUrl: 'congressionalDistrict'})
            .otherwise('/');
  }]);

