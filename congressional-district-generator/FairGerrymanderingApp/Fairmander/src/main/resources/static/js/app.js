var app = angular.module("App", ['ui-leaflet', 'chart.js', 'ui.bootstrap', 'ngAnimate', 'ngSanitize', 'ngRoute','rzModule']);
app.run(["$rootScope",'userFactory', function (rootScope,userFactory) {
  userFactory.getUser().then(function(data){
  rootScope.userInfo=data;
  });
  rootScope.stateMeasures={year:2016,compactness:0,geographic:0,partisanFairness:0,socioeconomic:0};
  rootScope.stateMDefaults={year:2016,compactness:0,geographic:0,partisanFairness:0,socioeconomic:0};
  }]);

app.factory('userFactory',['$http','$log',function(http,log){
  log.log("starting service");
  return{
    getUser:function(){
    return http.post('/user').then(function(response){
      var res=response.data;
      if(res.status==='Admin'){
        return { username: res.username,name: res.name, admin: false, registered: false};
      }
      else if(res.status==='User'){
        return { username: res.username, name: res.name, admin: true, registered: false};
      }
      else{
        return { username: "to BATSymandering",name: "", admin: true, registered: true};
      }
      
      },function(response){
        var res= response.data;
        return { username: "to BATSymandering",name: "", admin: true, registered: true};
      });
    }
  };
}]);

angular.module("data", ['chart.js']);
angular.module("data").controller("dataController", function ($scope) {
  $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
  $scope.data = [300, 500, 100];
  $scope.stateName = "ARKANSAS";
  $scope.year = 2016;
  $scope.partisanAlliance = "Republican";
  $scope.population = "3020327";
  $scope.efficiencyGap = 17;
  $scope.numCongDistricts = 3;
  $scope.numPrecincts = 147;
  $scope.racialComp = ["African-American", "Caucasian", "Asian", "Native", "Hispanic"];
  $scope.racialPercentage = [15.4, 77.0, 2.9, 0.8, 10.1];
  $scope.electionParties = ["Republican", "Democratic", "Other"];
  $scope.electionResults = [677904, 378729, 65051]
  $scope.compactness = 19.25235235232523141351;
  $scope.socioeconomicRanges = ["1-20000", "20000-40000", "40000-60000", "60000-80000", "80000-100000+"];
  $scope.socioeconomicPercentages = [39, 22, 19, 12, 10];
  //http get dataad send it through using angular extend.
});


app.controller("mapController", ["$scope", "$http","$rootScope", function (scope, http,rootScope) {
//    
//  scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
//  scope.data = [300, 500, 100];
//  scope.stateName = "ARKANSAS";
//  scope.year = 2016;
//  scope.partisanAlliance = "Republican";
//  scope.population = "3020327";
//  scope.efficiencyGap = 17;
//  scope.numCongDistricts = 3;
//  scope.numPrecincts = 147;
//  scope.racialComp = ["African-American", "Caucasian", "Asian", "Native", "Hispanic"];
//  scope.racialPercentage = [15.4, 77.0, 2.9, 0.8, 10.1];
//  scope.electionParties = ["Republican", "Democratic", "Other"];
//  scope.electionResults = [677904, 378729, 65051]
//  scope.compactness = 19.25235235232523141351;
//  scope.socioeconomicRanges = ["1-20000", "20000-40000", "40000-60000", "60000-80000", "80000-100000+"];
//  scope.socioeconomicPercentages = [39, 22, 19, 12, 10];

      scope.US={
        lat: 39.0,
        lng: -95,
        zoom: 5
      },
      scope.MA={
        lat:42.4,
        lng:-72.1,
        zoom:8
      };
      scope.defaults= {
        zoomControlPosition: 'bottomright',
        scrollWheelZoom: false
      };
scope.layers = {
        baselayers:{osm: {
                        name: 'OpenStreetMap',
                        url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        type: 'xyz'
                      }},
        overlays:{}
      };
    function redirectState(e) {
      var props = e.target.feature.properties;
      console.log(props.name);
      if (props.name === "Arkansas") {
        window.location.href = "./state?statename=Arkansas";
      } else if (props.name === "Massachusetts") {
        window.location.href = "./ma-state.html"; //./state?statename=Massachusetts
      } else if (props.name === "Connecticut") {
        window.location.href = "./state?statename=Connecticut";
      }
    }
    function onEachFeature(feature, layer) {
      layer.on('click', function (e)
      {
        redirectState(e);
      });
    }
    http.get("json/us-states.geo.json").then(function (data) {
      angular.extend(scope, {
        geojson: {
          data: data.data,
          style: {
            fillColor: "green",
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7
          },
          onEachFeature: onEachFeature
        }
      });
    });
    function getCongDistrictNum(n, congdis) {
      var t;
      for (var i = 0; i < congdis.length; i++) {
        t = congdis[i];
        for (var j = 0; j < t.length; j++) {
          if (t[j] == n) {
            return i;
          }
        }
      }
    }
    function getPrecinctColor(n) {
      return n === 0 ? '#FFB26D' :
              n === 1 ? '#008080' :
              n === 2 ? '#F6E000' :
              n === 3 ? '#FF1A00' :
              n === 4 ? '#FF00FF' :
              n === 5 ? '#841F27' :
              n === 6 ? '#FA8072' :
              n === 7 ? '#EB5E00' :
              n === 8 ? '#638C08' :
              '';
    }
    function styler(feature, num) {
      return {
        fillColor: getPrecinctColor(getCongDistrictNum(feature.properties.VTDST10, num)), //VTDST10
        weight: 1,
        opacity: 0.7,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
      };
    };
    http.get("/revised2.geojson").then(function (data) {
      http.get("/CDbyPrecinct.json").then(function(congressionalDistricts){
      rootScope.precinctlist = data;
      angular.extend(scope.layers.overlays, {
        ma_precincts:{
          name:'Default',
            type: 'geoJSONShape',
            data: data.data,
            visible: true,//geojsons
            layerOptions:{
            style: function (feature) {
              return styler(feature, congressionalDistricts.data.data);
            }}
          }});
        $('#loader').hide();
        },function (res) {
        console.log("Error " + res.status);
      });
    }, function (res) {
      console.log("Error " + res.status);
    });
    function startAlgorithm(){
      return http.post("/startAlgo",rootScope.stateMeasures).then(function (response) {
        return response;
      },function (res) {
        console.log("Error " + res.status);
      });
    }
    scope.runAlgorithm = function() {
      startAlgorithm().then(function(startAlgoResponse) {
        console.log(startAlgoResponse);
        if (startAlgoResponse.data.ready == "true") {
          
      http.get("/CDbyPrecinctStep1.json").then(function(congressionalDistricts){
      angular.extend(scope.layers.overlays, {
        ma_precincts2:{
          name:'Algorithm Layer',
            type: 'geoJSONShape',
            data: data.data,
            visible: true,//geojsons
            layerOptions:{
            style: function (rootScope.precinctList) {
              return reStyle(rootScope.precinctlist, congressionalDistricts.data.data);
            }}
          }
        });
        $('#loader').hide();
        },function (res) {
        console.log("Error " + res.status);
      });          
              });
            }

        }}, function (res) {
        console.log("Error " + res.status);
      });
    };
    function reStyle(changes) {
      return {
        fillColor: getPrecinctColor(changes.congressionalDistrict),
        weight: 1,
        opacity: 0.8,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
      };
    }
    ;
  }]);

app.controller("mapSidebarController",['$scope','$rootScope', function (scope,rootScope) {
  var sidebar = L.control.sidebar('sidebar').addTo(map);
  
  scope.reset=function(){
    rootScope.stateMeasures=rootScope.stateMDefaults;
    scope.compactnessRange.value=rootScope.stateMeasures.compactness;
    scope.geographicRange.value=rootScope.stateMeasures.compactness;
    scope.partisanFairnessRange.value=rootScope.stateMeasures.compactness;
    scope.socioeconomicRange.value=rootScope.stateMeasures.compactness;
    //rest of the measures values or booleans
  };
  scope.apply=function(){
    scope.runAlgorithm();
  };
  scope.updateCompactness=function(){
    rootScope.stateMeasures.compactness=scope.compactnessRange.value;
    console.log(rootScope.stateMeasures.compactness);
  };
  scope.updateGeo=function(){
    rootScope.stateMeasures.geo=scope.geographicRange.value;
    console.log(rootScope.stateMeasures.geo);
  };
  scope.updatePartisanFairness=function(){
    rootScope.stateMeasures.partisanFairness=scope.partisanFairnessRange.value;
    console.log(rootScope.stateMeasures.partisanFairness);
  };
  scope.updateSocioeconomic=function(){
    rootScope.stateMeasures.socioeconomic=scope.socioeconomicRange.value;
    console.log(rootScope.stateMeasures.socioeconomic);
  };
  
  scope.compactnessRange={
    value:rootScope.stateMeasures.compactness,
    options:{
      showTicksValues: true,
      floor: 0,
      ceil: 5,
      onChange:scope.updateCompactness
    }};
  scope.geographicRange={
    value:rootScope.stateMeasures.geographic,
    options:{
      showTicksValues: true,
      floor: 0,
      ceil: 5,
      onChange:scope.updateGeographic
    }};
  scope.partisanFairnessRange={
    value:rootScope.stateMeasures.partisanFairness,
    options:{
      showTicksValues: true,
      floor: 0,
      ceil: 5,
      onChange:scope.updatePartisanFairness
    }};
  
  scope.socioeconomicRange={
    value:rootScope.stateMeasures.socioeconomic,
    options:{
      showTicksValues: true,
      floor: 0,
      ceil: 5,
      onChange:scope.updateSocioeconomic
    }};
  
}]);

app.controller("yearController",['$scope','$rootScope',function(scope,rootScope){
  scope.updateYear=function(){
    rootScope.stateMeasures.year=scope.yearChoices.value;
    console.log(rootScope.stateMeasures.year);
    //set session to this year, or just keep it in rootScope to grab later when sending request for new state year.
  };
    scope.yearChoices={
      value: rootScope.stateMeasures.year,
      options: {
      showTicksValues: true,
      stepsArray:[
      {value: 1996},
      {value: 2000},
      {value: 2004},
      {value: 2008},
      {value: 2012},
      {value: 2016}
      ],
      onChange:scope.updateYear
  }};
}]);

app.controller('ModalCtrl', ['$scope', '$uibModal', '$log', '$document','logoutFactory','$rootScope', function (scope, modal, log, doc,logoutFactory,rootScope) {
    scope.user = { username: "",name: "", password: "", email: ""};    
    scope.animationsEnabled = true;
    scope.open = function (size, parentSelector) {
      var parentElem = parentSelector ? angular.element(doc[0].querySelector('.modal-position ' + parentSelector)) : undefined;
      var modalInstance = modal.open({
        animation: scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'myModalContent.html',
        controller: 'ModalInstanceCtrl',
        size: size,
        appendTo: parentElem
      });
      modalInstance.result.then(function () {
      }, function () {
        log.info('Modal dismissed at: ' + new Date());
      });
    };
    scope.openlogin = function (size, parentSelector) {
      var parentElem = parentSelector ?
              angular.element(doc[0].querySelector('.modal-position ' + parentSelector)) : undefined;
      var modalInstance = modal.open({
        animation: scope.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'loginModal.html',
        controller: 'ModalInstanceCtrl',
        size: size,
        appendTo: parentElem
      });
      modalInstance.result.then(function () {
      }, function () {
        log.info('Modal dismissed at: ' + new Date());
      });
    };
    scope.logout=function(){
      logoutFactory.getStatus().then(function(data){
      rootScope.userInfo=data;
      },function(){
      return { username: "to BATSymandering",name: "", admin: true, registered: true};
      });
    };
  }]);

app.factory('logoutFactory',['$http','$log',function(http,log){
  log.log("starting service");
  return{
    getStatus:function(){
    return http.post("/logout").then(function(response){
      var res=response.data;
        if(res.status==='success'){
          return { username: "to BATSymandering",name: "", admin: true, registered: true};
        }
        else{
          return { username: "to BATSymandering",name: "", admin: true, registered: true};
        }
      },function(){
        return { username: "to BATSymandering",name: "", admin: true, registered: true};
      });
    }
  };
}]);

app.controller('ModalInstanceCtrl', ['$rootScope', '$scope', '$http', '$uibModalInstance', function (rootScope, scope, http, modalInst) {
//    scope.login = function () {
//      login();
//      modalInst.close(scope);
//    };
    scope.login=function() {
      http.post('/login', scope.user).then(function (response) {
        var res = response.data;
        if (res.status === "Admin") {
          rootScope.userInfo.admin = false;
          rootScope.userInfo.registered = false;
          rootScope.userInfo.username=res.username;
          rootScope.userInfo.name = res.name;
          } 
          else if (res.status === "User") {
            rootScope.userInfo.registered = false;
            rootScope.userInfo.username=res.username;
            rootScope.userInfo.name = res.name;
            } 
          else {
            window.alert("wrong username or password!");
            }
          },function () {
              window.alert("Sorry server error!");
            });
            modalInst.close(scope);
    };
    scope.register = function () {
      http.post('/register', scope.user).then(function (response) {
        var res = response.data;
        if (res.status === "Username_Taken") {
          window.alert("Username was taken!");
        } else if (res.status === "success") {
          rootScope.userInfo.name = res.name;
          rootScope.userInfo.username= res.username;
        }
      }, function () {
        window.alert("Sorry server error!");
      });
      modalInst.close(scope);
    };
    scope.cancel = function () {
      modalInst.dismiss('cancel');
    };
  }]);

app.controller('account',['$scope','$rootScope','$http',function(scope,rootScope,http){
    scope.user = {name: "", userName: "",oldPassword: "", newPassword: ""};
    scope.adminUser = {name: "", userName: "",password: "", email: ""};
    scope.userlist=["tombat","alice","alicelovesbob","jack","jon"];
    scope.duser="";
    scope.addUser=function(){
//      scope.userlist.push(scope.adminUser.username);
      console.log("got here");
      console.log(scope.adminUser);
      http.post('/register',scope.adminUser).then(function(response){
        var res=response.data;
       if(res.status==="success"){
         scope.userlist.push(res.username);
         scope.resetForms;
    }
    else{
      window.alert("User already exists!");
    }
      },function(){
        window.alert("Server Error!");
      });
    };
    scope.deleteUser=function(){
      var titlle=scope.adminUser.userName;
      http.post("/deleteUser",titlle).then(function(response){
        var res=response.data;
        if(res.status==='No')
          window.alert("User doesn't exist");
        else{
          scope.userlist.pop(res.username);
          scope.resetForms;
        }
      },function(){
        window.alert("Server Error!");
      });
    };
    scope.updateUser=function(){
      http.post('/editUser',scope.adminUser).then(function(response){
        var res=response.data;
        if(res.status=== "edited"){
          window.alert("Successfully edited user!");
        }
        else{
          window.alert("User doesn't exist!");
        }
      },function(){
        window.alert("Server Error!");
      });
    };
    scope.resetForms=function(){
     scope.adminUser = {name: "", username: "",password: "", email: ""};
    };
    scope.changeInfo=function(){
      if(scope.user.name===""){
        console.log(rootScope.userInfo.name);
        scope.user.name=rootScope.userInfo.name;
      }
      if(scope.user.username===""){
        console.log(rootScope.userInfo.username);
        scope.user.username=rootScope.userInfo.username;
      }
      
      http.post("/account",scope.user).then(function(response){
        var res=response.data;
        console.log(res);
        if(res.password==='success'){
          rootScope.userInfo.name=res.name;
          rootScope.userInfo.username=res.username;
          window.alert("Account changes successfully saved!");
        }
        else{
          window.alert("Authentification failed!");
        }
        
      },function(){
        window.alert("Server Error!");
      });
    };
}]);

app.controller('congressDistricter',['$scope','$rootScope','$http','$location',function(scope,rootScope,http,location){
    scope.cData={pop:"N/A",rp:"N/A",rc:"N/A",dem:"N/A",rep:"N/A",lib:"N/A",gr:"N/A",oth:"N/A"};
    scope.switchCd = function() {
    var Cdd = scope.selected;
    switch (Cdd) {
      case '1':
        http.post('/cdData',Cdd).then(function(response){
          var res=response.data;
          console.log(res);
          scope.cData.pop=res.population;
          scope.cData.rp=res.racialPercentage;
          scope.cData.rc=res.racialComp;
          scope.cData.dem=res.democratic;
          scope.cData.rep=res.republican;
          scope.cData.lib=res.libertarian;
          scope.cData.gr=res.green;
          scope.cData.oth=res.other;
        },function(response){
          windows.alert("Server Error!");
        });
        break;
      case '2':
          http.post('/cdData',Cdd).then(function(response){
          var res=response.data;
          scope.cData.pop=res.population;
          scope.cData.rp=res.racialPercentage;
          scope.cData.rc=res.racialComp;
          scope.cData.dem=res.democratic;
          scope.cData.rep=res.republican;
          scope.cData.lib=res.libertarian;
          scope.cData.gr=res.green;
          scope.cData.oth=res.other;
        },function(response){
          windows.alert("Server Error!");
        });
        break;
      case '3':
        http.post('/cdData',Cdd).then(function(response){
          var res=response.data;
          scope.cData.pop=res.population;
          scope.cData.rp=res.racialPercentage;
          scope.cData.rc=res.racialComp;
          scope.cData.dem=res.democratic;
          scope.cData.rep=res.republican;
          scope.cData.lib=res.libertarian;
          scope.cData.gr=res.green;
          scope.cData.oth=res.other;
        },function(response){
          windows.alert("Server Error!");
        });
        break;
      case '4':
        http.post('/cdData',Cdd).then(function(response){
          var res=response.data;
          scope.cData.pop=res.population;
          scope.cData.rp=res.racialPercentage;
          scope.cData.rc=res.racialComp;
          scope.cData.dem=res.democratic;
          scope.cData.rep=res.republican;
          scope.cData.lib=res.libertarian;
          scope.cData.gr=res.green;
          scope.cData.oth=res.other;
        },function(response){
          windows.alert("Server Error!");
        });
        break;
      case '5':
        http.post('/cdData',Cdd).then(function(response){
          var res=response.data;
          scope.cData.pop=res.population;
          scope.cData.rp=res.racialPercentage;
          scope.cData.rc=res.racialComp;
          scope.cData.dem=res.democratic;
          scope.cData.rep=res.republican;
          scope.cData.lib=res.libertarian;
          scope.cData.gr=res.green;
          scope.cData.oth=res.other;
        },function(response){
          windows.alert("Server Error!");
        });
        break;
      case '6':
        http.post('/cdData',Cdd).then(function(response){
          var res=response.data;
          scope.cData.pop=res.population;
          scope.cData.rp=res.racialPercentage;
          scope.cData.rc=res.racialComp;
          scope.cData.dem=res.democratic;
          scope.cData.rep=res.republican;
          scope.cData.lib=res.libertarian;
          scope.cData.gr=res.green;
          scope.cData.oth=res.other;
        },function(response){
          windows.alert("Server Error!");
        });
        break;
      case '7':
        http.post('/cdData',Cdd).then(function(response){
          var res=response.data;
          scope.cData.pop=res.population;
          scope.cData.rp=res.racialPercentage;
          scope.cData.rc=res.racialComp;
          scope.cData.dem=res.democratic;
          scope.cData.rep=res.republican;
          scope.cData.lib=res.libertarian;
          scope.cData.gr=res.green;
          scope.cData.oth=res.other;
        },function(response){
          windows.alert("Server Error!");
        });
        break;
      case '8':
        http.post('/cdData',Cdd).then(function(response){
          var res=response.data;
          scope.cData.pop=res.population;
          scope.cData.rp=res.racialPercentage;
          scope.cData.rc=res.racialComp;
          scope.cData.dem=res.democratic;
          scope.cData.rep=res.republican;
          scope.cData.lib=res.libertarian;
          scope.cData.gr=res.green;
          scope.cData.oth=res.other;
        },function(response){
          windows.alert("Server Error!");
        });
        break;
      case '9':
        http.post('/cdData',Cdd).then(function(response){
          var res=response.data;
          scope.cData.pop=res.population;
          scope.cData.rp=res.racialPercentage;
          scope.cData.rc=res.racialComp;
          scope.cData.dem=res.democratic;
          scope.cData.rep=res.republican;
          scope.cData.lib=res.libertarian;
          scope.cData.gr=res.green;
          scope.cData.oth=res.other;
        },function(response){
          windows.alert("Server Error!");
        });
        break;
      default:
    }
  };
}]);