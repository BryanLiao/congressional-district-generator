<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><spring:url value="/css/index.css" var="springCss" /> 
<h1>Statistics</h1> 
<div id=dataBlock> 
<!--  <canvas id="pie" class="chart chart-pie" 
          chart-data="socioeconomicPercentages" chart-labels="socioeconomicRanges" chart-options="options"> 
  </canvas> -->
  <canvas id="bar" class="chart chart-bar" 
          chart-data="racialPercentage" chart-labels="racialComp" chart-series="series"> 
  </canvas> 
     
    <canvas id="bar" class="chart chart-bar" 
            chart-data="${racialPercentage}" chart-labels="${racialComp}" chart-series="series"> 
    </canvas> 
    Statistics List: ${statename}<br> 
    <br> 
    Population: ${population}<br> 
    <br> 
    Partisan Values<br> 
    Democratic: ${democratic}<br> 
    Republican: ${republican}<br> 
    Libertarian: ${libertarian}<br> 
    Green: ${green}<br> 
    Other: ${other}<br> 
    <br> 
    ${partnum}<br> 
</div> 
